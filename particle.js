function Particle () {
  this.pos  = createVector(random(0, width), random(0, height)); // position
  this.vel = createVector(0, 0); // velosity
  this.acc = createVector(0, 0); // acceleration
  this.maxSpeed = 2;

  this.prevPos = this.pos.copy();

  this.update = function() {

    this.vel.add(this.acc);
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel);
    this.acc.mult(0);

  };

  this.follow = function (vectors) {

    // scaled position
    var x = floor (this.pos.x / scl);
    var y = floor (this.pos.y / scl);

    // cast to position in array of directions
    var index = x + y * cols;

    // get the influencing direction => force
    var force = vectors[index];

    this.applyForce(force);

  };

  this.applyForce = function (force) {
    this.acc.add(force);

  };

  this.show = function () {
    stroke(255, 5);
    strokeWeight(4);
    // point(this.pos.x, this.pos.y);

    line(this.pos.x, this.pos.y, this.prevPos.x, this.pos.y);

    this.updatePrev();
  };

  this.updatePrev = function () {
    this.prevPos.x = this.pos.x;
    this.prevPos.y = this.pos.y;
  }

  this.edges = function () {
    if (this.pos.x > width) {
      this.pos.x = 0;
      this.updatePrev();
    }
    if (this.pos.x < 0) {
      this.pos.x = width;
      this.updatePrev();
    }
    if (this.pos.y > height) {
      this.pos.y = 0;
      this.updatePrev();
    }
    if (this.pos.y < 0) {
      this.pos.y = height;
      this.updatePrev();
    }

  };
}
