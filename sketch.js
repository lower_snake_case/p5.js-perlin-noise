var inc   = 0.1;
var scl   = 10; // scale
var zoff  = 0;
var cols;
var rows;
var particles = [];
var flowField;


function setup() {
  createCanvas(1920, 1080);

  cols = floor (width / scl); // cols per width
  rows = floor (height / scl); // rows per height

  // one vector per scale
  fr = createP('');

  flowField = new Array (cols * rows);

  for (var i = 0; i < 500; ++i) {
    particles [i] = new Particle();
  }

  background(51);

}

function draw() {

  // noiseDetail(12, 0.5);

  var yoff = 0;

  for (var y = 0; y < rows; y++) {

    var xoff = 0;

    for (var x = 0; x < cols; x++) {
      var index = (x + y * cols);
      var angle = noise(xoff, yoff, zoff) * TWO_PI * 4;
      var v = p5.Vector.fromAngle(angle);

      v.setMag(0.5);

      flowField[index] = v;

      xoff += inc;


      // stroke(0, 50);
      // strokeWeight(1);
      // push();
      // translate(x* scl, y *scl);
      // rotate(v.heading());
      // line(0, 0, scl, 0);


      // pop();
    }
    yoff += inc;


  }
  zoff += 0.003;

  for (var i = 0; i < particles.length; i++) {
    particles[i].follow(flowField);
    particles[i].update();
    particles[i].edges();
    particles[i].show();
  }

  // noLoop();
  fr.html(floor(frameRate()));

}
